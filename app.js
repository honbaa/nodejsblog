var express = require('express');
// 加载模板处理模块
var swig = require('swig');
var app = express();

// 设置静态文件托管
app.use('/public',express.static(__dirname + '/public'));
// 定义模板引擎，第一个参数是模板文件扩展名，第二个参数是解析模板函数
app.engine('html',swig.renderFile);
// 设置模板文件存放目录，第一个参数必须是views，第二个是文件目录
app.set('views','./views');
// 注册模板引擎，第一个参数必须是view engine, 第二个参数必须是模板文件扩展名
app.set('view engine','html');
// 开发过程中将缓存设置为false
swig.setDefaults({cache:false})

// 登录
app.get('/',function(req,res,next){
    //res.send("欢迎进入博客！");
    // 读取views目录下的模板文件，并解析成HTML返回给客户
    res.render('index');
});

app.listen(8080);